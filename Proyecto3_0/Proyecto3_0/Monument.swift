//
//  Monument.swift
//  Proyecto3_0
//
//  Created by dedam on 9/1/18.
//  Copyright © 2018 dedam. All rights reserved.
//

import UIKit

class Monument {
    var nombre:String
    var ciudad:String
    var lat:Float
    var lon:Float
    var distancia:Float
    var pitch:Float
    var heading:Float
    
    init() {
        nombre = ""
        ciudad = ""
        lat = 0.0
        lon = 0.0
        distancia = 0
        pitch = 0
        heading = 0
    }
    
    init(_ nombre:String, _ ciudad:String, _ lat:Float, _ lon:Float, _ distancia:Float, _ pitch:Float, _ heading:Float ) {
        self.nombre     = nombre
        self.ciudad     = ciudad
        self.lat        = lat
        self.lon        = lon
        self.distancia  = distancia
        self.pitch      = pitch
        self.heading    = heading
    }
    
}
