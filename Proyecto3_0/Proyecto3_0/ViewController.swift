//
//  ViewController.swift
//  Proyecto3_0
//
//  Created by dedam on 9/1/18.
//  Copyright © 2018 dedam. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var lbl_ciudad: UILabel!
    
    @IBOutlet weak var monumentsMap: MKMapView!
    
    
    @IBOutlet weak var ReferenceMap: MKMapView!
    
    @IBOutlet weak var btn_Next: UIButton!
    @IBOutlet weak var btn_OK: UIButton!
    @IBOutlet weak var lbl_central: UILabel!
    @IBOutlet weak var lbl_Km: UILabel!
    
    var myMonumentos:[Monument] = []
    
    var currentMonumento:Monument?
    
    var tapLocation:CLLocationCoordinate2D?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tapLocation = nil
        ReferenceMap.delegate = self
        
        
        currentMonumento = nil
        initJuego ()
        
        tapRespuesta()
        
    }
    
    
    
    func tapRespuesta(){
        
        let tapG = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(sender:)))
        
        ReferenceMap.addGestureRecognizer(tapG)
        tapG.numberOfTapsRequired = 3
        
        /*let long = UILongPressGestureRecognizer(target: self, action: #selector(self.myLongPressGesture(sender:)))
         long.numberOfTapsRequired = 1
         long.minimumPressDuration = 1
         long.allowableMovement = 100
         self.ReferenceMap.addGestureRecognizer(long)
 */
    }
    /*@objc func myLongPressGesture (sender: UILongPressGestureRecognizer) {
     
     lprint("Please Help!")
     
     let points = sender.location(in: self.ReferenceMap)
     
     tapLocation = ReferenceMap.convert(points, toCoordinateFrom: self.ReferenceMap)
     
     let pin = MKPointAnnotation()
     pin.coordinate = tapLocation!
     pin.title = "Tu Respuesta"
     //pin.subtitle = "ciudad"
     ReferenceMap.addAnnotation(pin)
     
     }
 */
    @objc func tapBlurButton(sender: UITapGestureRecognizer){
        
        print("Please Help!")
        
        let points = sender.location(in: self.ReferenceMap)
        
        tapLocation = ReferenceMap.convert(points, toCoordinateFrom: self.ReferenceMap)
        
        let pin = MKPointAnnotation()
        pin.coordinate = tapLocation!
        pin.title = "Tu Respuesta"
        //pin.subtitle = "ciudad"
        ReferenceMap.addAnnotation(pin)
        
    }
    
    func initJuego () {
        initMonumentos()
        selectRandomMonumento()
        mostrarMonumento()
        nextMap()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBAction func OK_pressed(_ sender: Any) {
        
        if (tapLocation != nil){
            
            print("altitude: \(monumentsMap.camera.altitude)")
            print("pitch: \(monumentsMap.camera.pitch)")
            print("heading: \(monumentsMap.camera.heading)")
            print("latitude: \(monumentsMap.camera.centerCoordinate.latitude)")
            print("longitude: \(monumentsMap.camera.centerCoordinate.longitude)")
            
            setRegion()
            
            createPolyline()
            
            labelsMonuments()
            
            let pin = MKPointAnnotation()
            pin.coordinate = CLLocationCoordinate2D(latitude: (Double) ((currentMonumento?.lat)!), longitude: (Double) ((currentMonumento?.lon)!))
            pin.title = "\(currentMonumento!.nombre)"
            pin.subtitle = "\(currentMonumento!.ciudad)"
            self.ReferenceMap.addAnnotation(pin)
            
            
            
        }else{
            
            
        }
        
    }
    
    func nextMap(){
        ReferenceMap.mapType = MKMapType.standard
        ReferenceMap.isZoomEnabled = true
        ReferenceMap.showsBuildings = false
        ReferenceMap.showsPointsOfInterest = false
        /*let camera = mapMonuments.camera
         
         mapMonuments.camera.heading = (Double) ((currentMonumento?.heading)!)
         mapMonuments.camera.pitch = (CGFloat) ((currentMonumento?.pitch)!)
         mapMonuments.camera.altitude = (Double) ((currentMonumento?.distancia)!)
         mapMonuments.camera.centerCoordinate = CLLocationCoordinate2D(latitude: (Double) ((currentMonumento?.lat)!), longitude: (Double) ((currentMonumento?.lon)!))
         */
        let coordinate = CLLocationCoordinate2D(latitude: (Double) (40.0000000), longitude: (Double) (-4.0000000))
        
        let camera = MKMapCamera(lookingAtCenter: coordinate,
                                 fromDistance: (Double) (1500000),
                                 pitch: (CGFloat) (0),
                                 heading: (Double) (0))
        ReferenceMap.camera = camera
        
        ReferenceMap.delegate = self
        
    }
    
    
    
    func setRegion(){
        ReferenceMap.mapType = MKMapType.standard
        ReferenceMap.isZoomEnabled = true
        ReferenceMap.showsBuildings = false
        ReferenceMap.showsPointsOfInterest = false
        
        /*let camera = mapMonuments.camera
         
         mapMonuments.camera.heading = (Double) ((currentMonumento?.heading)!)
         mapMonuments.camera.pitch = (CGFloat) ((currentMonumento?.pitch)!)
         mapMonuments.camera.altitude = (Double) ((currentMonumento?.distancia)!)
         mapMonuments.camera.centerCoordinate = CLLocationCoordinate2D(latitude: (Double) ((currentMonumento?.lat)!), longitude: (Double) ((currentMonumento?.lon)!))
         */
        let coordinate = CLLocationCoordinate2D(latitude: (Double) ((currentMonumento?.lat)!), longitude: (Double) ((currentMonumento?.lon)!))
        
        let camera = MKMapCamera(lookingAtCenter: coordinate,
                                 fromDistance: (Double) (10000000),
                                 pitch: (CGFloat) (0),
                                 heading: (Double) (0))
        ReferenceMap.camera = camera
        
        
        
    }
    
    
    @IBAction func next_pressed(_ sender: Any) {
        selectRandomMonumento()
        mostrarMonumento()
        nextMap()
        tapRespuesta()
        clearLabels()
       
        
        ReferenceMap.removeAnnotations(ReferenceMap.annotations)
        
        
        ReferenceMap.removeOverlays(ReferenceMap.overlays)
        
        
    }
    
    func mostrarMonumento() {
        monumentsMap.mapType = MKMapType.satelliteFlyover
        // mapMonuments.isZoomEnabled = false
        monumentsMap.showsBuildings = true
        monumentsMap.showsPointsOfInterest = false
        
        
        /*let camera = mapMonuments.camera
         
         mapMonuments.camera.heading = (Double) ((currentMonumento?.heading)!)
         mapMonuments.camera.pitch = (CGFloat) ((currentMonumento?.pitch)!)
         mapMonuments.camera.altitude = (Double) ((currentMonumento?.distancia)!)
         mapMonuments.camera.centerCoordinate = CLLocationCoordinate2D(latitude: (Double) ((currentMonumento?.lat)!), longitude: (Double) ((currentMonumento?.lon)!))
         */
        let coordinate = CLLocationCoordinate2D(latitude: (Double) ((currentMonumento?.lat)!), longitude: (Double) ((currentMonumento?.lon)!))
        
        let camera = MKMapCamera(lookingAtCenter: coordinate,
                                 fromDistance: (Double) ((currentMonumento?.distancia)!),
                                 pitch: (CGFloat) ((currentMonumento?.pitch)!),
                                 heading: (Double) ((currentMonumento?.heading)!))
        monumentsMap.camera = camera
        
    }
    
    func selectRandomMonumento(){
        let index = randomInt(min:0, max:myMonumentos.count-1)
        currentMonumento = myMonumentos[3]
        myMonumentos.remove(at: 3)
    }
    
    func randomInt(min: Int, max:Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    
    func clearLabels () {
        
        self.lbl_central.text = "Situa el monumento en el mapa"
        
        self.lbl_ciudad.text = ""
        
        self.lbl_Km.text = "0 Km"
    }
    
    func labelsMonuments () {
        
        self.lbl_central.text = "\(currentMonumento!.nombre)"
        
        self.lbl_ciudad.text = "\(currentMonumento!.ciudad)"
        
        
        
    }
    
    func createPolyline() {
        let point1 = CLLocationCoordinate2DMake((Double) ((currentMonumento?.lat)!), (Double) ((currentMonumento?.lon)!));
        let point2 = CLLocationCoordinate2DMake((Double) ((tapLocation?.latitude)!), (Double) ((tapLocation?.longitude)!));
        
        let points: [CLLocationCoordinate2D]
        points = [point1, point2]
        
        let geodesic = MKGeodesicPolyline(coordinates: points, count: 2)
        ReferenceMap.add(geodesic)
        
        UIView.animate(withDuration: 1.5, animations: { () -> Void in
            let span = MKCoordinateSpanMake(0.1, 0.1)
            let region1 = MKCoordinateRegion(center: point1, span: span)
            self.ReferenceMap.setRegion(region1, animated: true)
        })
        
        let myLocation = CLLocation(latitude: (Double) ((currentMonumento?.lat)!), longitude: (Double) ((currentMonumento?.lon)!))
        
        //My buddy's location
        let myBuddysLocation = CLLocation(latitude: (Double) ((tapLocation?.latitude)!), longitude: (Double) ((tapLocation?.longitude)!))
        
        //Measuring my distance to my buddy's (in km)
        let distance = myLocation.distance(from: myBuddysLocation) / 1000
        
        //Display the result in km
        print(String(format: "The distance to my buddy is %.01fkm", distance))
        self.lbl_Km.text = "\((Int) (distance))Km"
    }
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = UIColor.blue
        return renderer
        
    }
    
        
    
    
    func initMonumentos()
    {
        
        
        
        //_ nombre:String, _ ciudad:String, _ lat:Float, _ lon:Float, _ distancia:Float, _ pitch:Float, _ heading:Float
        myMonumentos.append(Monument.init("La Sagrada Familia", "BARCELONA", 41.4028931, 2.1719068, 450, 80, 70))
        myMonumentos.append(Monument.init("La Puerta de Alcalá", "MADRID", 40.420788, -3.688876, 200, 25, 230))
        myMonumentos.append(Monument.init("Empire State", "NEW YORK", 40.748327, -73.985471, 925, 45, 170))
        myMonumentos.append(Monument.init("La Torre Eiffel", "PARÍS", 48.8583701, 2.2922926, 1200, 60, 60))
        myMonumentos.append(Monument.init("El Coliseo", "ROMA", 41.8902102, 12.4900422, 250, 80, 75))
        myMonumentos.append(Monument.init("La Casa Blanca", "WASHINGTON", 38.8976815, -77.0368423, 500, 45, 0))
        myMonumentos.append(Monument.init("El Big Ben", "LONDRES", 51.5007292, -0.1268141, 550, 80, 260))
        myMonumentos.append(Monument.init("El Kremlin", "MOSCÚ", 55.751382, 37.618446, 600, 30, 280))
        myMonumentos.append(Monument.init("Tokyo Tower", "TOKYO", 35.6585805, 139.7448857, 900, 45, 0))
        myMonumentos.append(Monument.init("La Opera", "SIDNEY", -33.857033, 151.215191, 500, 45, 110))
        myMonumentos.append(Monument.init("El Partenón", "ATENES", 37.971402, 23.726591, 500, 65, 0))
        myMonumentos.append(Monument.init("Plaza de la Constitución", "MEXICO DF", 19.4319642, -99.1333981, 500, 45, 0))
        myMonumentos.append(Monument.init("Santa Sofía", "ISTANBUL", 41.005270, 28.976960, 500, 45, 0))
        myMonumentos.append(Monument.init("La Puerta de Brandenburgo", "BERLÍN", 52.5162746, 13.3755153, 400, 75, 260))
        myMonumentos.append(Monument.init("La Plaza de Mayo", "BUENOS AIRES", -34.6080556, -58.3724665, 500, 45, 75))
    }
}




